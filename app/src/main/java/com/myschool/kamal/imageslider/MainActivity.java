package com.myschool.kamal.imageslider;

import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
ArrayList<Integer> list=new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
private  int[]images={R.drawable.imag2,R.drawable.image,R.drawable.images,R.drawable.img};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    private  void init(){
        for(int i=0;i<images.length;i++){
        list.add(images[i]);
        mPager=findViewById(R.id.pager);
        mPager.setAdapter(new Adapter(list,MainActivity.this));
            CirclePageIndicator circlePageIndicator=findViewById(R.id.indicator);
            circlePageIndicator.setViewPager(mPager);
            final  float density=getResources().getDisplayMetrics().density;
            circlePageIndicator.setRadius(5*density);
            NUM_PAGES=images.length;
            final Handler handler=new Handler();
            final Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    if(currentPage==NUM_PAGES){
                        currentPage=0;
                    }
                    mPager.setCurrentItem(currentPage+1, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(runnable);
                }
            }, 1000,1000);
            circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });
        }
    }
}
