package com.myschool.kamal.imageslider;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class Adapter extends PagerAdapter {
    ArrayList<Integer> list;
    LayoutInflater inflater;
    Context context;

    public Adapter(ArrayList<Integer> list,  Context context) {
        this.list = list;
        this.inflater = inflater.from(context);
        this.context = context;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View imagelyout=inflater.inflate(R.layout.fragment_image,container,false);
        assert imagelyout != null;
        final ImageView imageView = (ImageView) imagelyout
                .findViewById(R.id.image);


        imageView.setImageResource(list.get(position));

        container.addView(imagelyout, 0);

        return imagelyout;

    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
